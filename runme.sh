#!/bin/sh

here="$(cd "$(dirname "$0")" && pwd)"
set -x

cd "${here}"

set -e

poetry build

# This craziness finds and installs just the "latest" wheel (i.e., that whose name sorts last alphabetically)
# https://unix.stackexchange.com/a/71385
wheels=( dist/example_package*.whl )
poetry run pip install ${wheels[${#wheels[@]}-1]}

poetry run python -m example_package
tox
